


var getSelect = () => {
  var result = getSelectValue("typeswitcher");
  var windows = document.getElementsByClassName('types')

  for(let i = 0; i < windows.length; i++) {
    if(i == result - 1){
      windows[i].classList.add('seetypes')
    }
    else if (windows[i].classList.contains('seetypes')){
      windows[i].classList.remove('seetypes')
    }
  }
}

var getSelectValue = (name) => {
  var e = document.getElementById(name)
  return e.options[e.selectedIndex].value;
}

var sendData = () => {
  
  const data = {
    sku: document.getElementById('sku').value,
    name: document.getElementById('name').value,
    price: document.getElementById('price').value,
    type: getSelectValue("typeswitcher"),
    size1: document.getElementById('size1').value,
    height1: document.getElementById('height1').value,
    width1: document.getElementById('width1').value,
    length1: document.getElementById('length1').value,
    weight1: document.getElementById('weight2').value,
  }
  // console.log(data)

  //========================================//
  axios.post('./makeRecord.php', data)
  .then(function (response) {
    console.log(response.config.data);
  })
  .catch(function (error) {
    console.log(error);
  });
}

