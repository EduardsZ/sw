<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>product add page</title>
  <link rel="stylesheet" href="./addItem.css" class="css">
  <link rel="stylesheet" href="./assets/bootstrap.min.css">
  <script src="./assets/axios.min.js"></script>
  <link rel="stylesheet" href="./productList.css">
</head>
<body>
  <div class="container mt-4">
  <header class="row">
    <h1>Product List</h1>
    <div class="form-inline">
      <select class="form-control mr-2" name="me" id="del">
        <option value="1">Mass delete action</option>
      </select>
      <button class="btn btn-info" onclick="sendData()">Apply</button>
    </div>
  </header>
  <main>
  <hr>
  <div class="items" id="items">
      <div class="card product_item">
        <input class="ml-2 mt-2" type="checkbox" name="me" id="">
        <div class="card-body text-center">
          <h5 class="card-title">JVC200123</h5>
          <h6 class="card-subtitle mb-2 text-muted">Acme Disc</h6>
          <p class="card-text">100 $</p>
          <p class="card-text">Size: 700 MB</p>
        </div>
      </div>
      <div class="card product_item">
        <input class="ml-2 mt-2" type="checkbox" name="me" id="">
        <div class="card-body text-center">
          <h5 class="card-title">JVC200123</h5>
          <h6 class="card-subtitle mb-2 text-muted">Acme Disc</h6>
          <p class="card-text">100 $</p>
          <p class="card-text">Size: 700 MB</p>
        </div>
      </div>
    </div>
  </main>
  <aside>
   
  </aside>
  </div>
  <script src="./productList.js"></script>
</body>
</html>