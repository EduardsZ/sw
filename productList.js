
getItems();

async function getItems() {
  try {
    const response = await axios.get('getItems.php');
    renderItems(response.data);
  } catch (error) {
    console.error(error);
  }
}

function renderItems(data) {
  let out = ''
  data.forEach(element => {
    out += '<div class="card product_item">'
    out += `<input class="ml-2 mt-2 fordelete" type="checkbox" name="me" data-id="${element.id}"></input>`
    out += '<div class="card-body text-center">'
    out += `<h5 class="card-title"> ${element.sku} </h5>`
    out += `<h6 class="card-subtitle mb-2 text-muted"> ${element.name} </h6>`
    out += `<p class="card-text"> ${element.price} </p>`
    if (element.type == 1)
    out += `<p class="card-text">Size: ${element.size}</p>`
    else if (element.type == 2)
    out += `<p class="card-text">Dimension: ${element.length} x ${element.width} x ${element.height} </p>`
    else if (element.type == 3)
    out += `<p class="card-text">Weight: ${element.weight}</p>`
  
    out += '</div></div>'
  });
  document.getElementById('items').innerHTML = out
}

async function sendData() {
  var elements = document.getElementsByClassName('fordelete')
  var ids = []
  Array.from(elements).forEach(el => {
    if(el.checked) {
      ids.push(el.dataset.id)
    }
  })
  if(ids.length) {
    
    try {
      const response = await axios.post('deleteItems.php', {
        data: ids
      } );
      getItems()
    } catch (error) {
      console.error(error);
    }
  }
}