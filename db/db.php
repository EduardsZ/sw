<?php

  require_once dirname(__FILE__) . '/../funcs.php';
  require_once dirname(__FILE__) . '/dbn.php';
  require_once dirname(__FILE__) . '/rb-mysql.php';

  try{
    R::setup( "mysql:host=127.0.0.1;dbname=$db_name", $db_user, $db_pass );
  }
  catch(PDOException $e){
      echo $e->getmessage();
  }

  if (!R::testConnection()){
    die("No connection");
  }

  // echo 'ok';