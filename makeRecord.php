<?php
require_once dirname(__FILE__) . '/db/db.php';

$data = json_decode(file_get_contents('php://input'), true);
$errors = [];

if(!isset($data['type']) or $data['type'] == 0 or $data['type'] == false){
  $errors[] = 'Inconsistent type';
}

if(empty($data['sku']) or empty($data['name']) or empty($data['price'])) {
  $errors[] = 'No sufficient data';
}
// here can be more checks...

if($errors){
  ddv($errors, 1, 1); // Just respond errors in JSON format
}
else {
  $item = R::dispense( 'items' );
  $item->sku = $data['sku'];
  $item->name = $data['name'];
  $item->price = $data['price'];
  $item->type = $data['type'];
  if ($data['type'] == 1) {
     $item->size = $data['size1'];
  } elseif ($data['type'] == 2) {
    $item->height = $data['height1'];
    $item->width = $data['width1'];
    $item->length = $data['length1'];
  } elseif ( $data['type'] == 3) {
    $item->weight = $data['weight1'];
  } else {
    // currently nothing to add
  }
  $item_id = R::store($item);
  echo $item_id;
  
}