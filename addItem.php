<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>product add page</title>
  <link rel="stylesheet" href="./addItem.css" class="css">
  <link rel="stylesheet" href="./assets/bootstrap.min.css" >
  <script src="./assets/axios.min.js"></script>
</head>
<body>
  <div class="container mt-4">
  <header class="row">
    <h1>Product Add</h1>
    <button class="btn btn-success" onclick="sendData()">Save</button>
  </header>
  <main>
    <hr>
    <div id="reply"></div>
    <label for="sku">SKU</label>
    <input type="text" name="sku" data-unit="sku" id="sku" required><br><br>
    <label for="name">Name</label>
    <input type="text" name="name" id="name" required><br><br>
    <label for="price">Price</label>
    <input type="text" name="price" id="price" required><br><br>
    <label for="typeswitcher">Type switcher</label>
    <select name="typeswitcher" id="typeswitcher" onchange="getSelect()">
      <option value="0" default>Select type</option>
      <option value="1">DVD Disk</option>
      <option value="2">Furniture</option>
      <option value="3">Book</option>
    </select>
  </main>
  <aside>
    <div class="types" id="type1">
      <label for="size1">Size</label>
      <input type="text" id="size1">
      <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consequatur eveniet debitis eum ea, harum ratione dolor numquam eius aut perspiciatis repellendus eos et, molestias iste nesciunt nulla alias praesentium sed!</p>
    </div>

    <div class="types" id="type2">
      <label for="height1">Height</label>
      <input type="text" id="height1"><br><br>
      <label for="width1">Width</label>
      <input type="text" id="width1"><br><br>
      <label for="length1">Length</label>
      <input type="text" id="length1">

      <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Explicabo officia, cumque repellat labore necessitatibus beatae impedit accusantium voluptatem dolorem nihil?</p>
    </div>

    <div class="types" id="type3">
    <label for="weight2">Weight</label>
      <input type="text" id="weight2">
      <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consequatur eveniet debitis eum ea, harum ratione dolor numquam eius aut perspiciatis repellendus eos et, molestias iste nesciunt nulla alias praesentium sed!</p>
    </div>
  </aside>
  </div>
  <script src="./addItem.js"></script>
</body>
</html>