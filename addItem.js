


var getSelect = () => {
  var result = getSelectValue("typeswitcher");
  var windows = document.getElementsByClassName('types')

  for(let i = 0; i < windows.length; i++) {
    if(i == result - 1){
      windows[i].classList.add('seetypes')
    }
    else if (windows[i].classList.contains('seetypes')){
      windows[i].classList.remove('seetypes')
    }
  }
}

var getSelectValue = (name) => {
  var e = document.getElementById(name)
  return e.options[e.selectedIndex].value;
}

var sendData = () => {
  let errors = []
  let reply = document.getElementById('reply')

  // Grab data from form
  const data = {
    sku: document.getElementById('sku').value,
    name: document.getElementById('name').value,
    price: document.getElementById('price').value,
    type: getSelectValue("typeswitcher"),
    size1: document.getElementById('size1').value,
    height1: document.getElementById('height1').value,
    width1: document.getElementById('width1').value,
    length1: document.getElementById('length1').value,
    weight1: document.getElementById('weight2').value,
  }
  
  // Here are the validations
  if(data.sku.length < 1) errors.push('SKU incorrect')
  if(data.name.length < 1) errors.push('Name incorrect')
  if(data.price.length < 1) errors.push('Price incorrect')
  if(data.type == 1 && data.size1.length < 1) errors.push('Size not present properly')
  if(data.type == 2 && data.height1.length < 1) errors.push('Height not present properly')
  if(data.type == 2 && data.width1.length < 1) errors.push('Width not present properly')
  if(data.type == 2 && data.length1.length < 1) errors.push('Length not present properly')
  if(data.type == 3 && data.weight1.length < 1) errors.push('Weight not present properly')
  //========================================//
  if(!errors.length){
    reply.innerHTML = ''
    axios.post('./makeRecord.php', data)
    .then(function (response) {
      console.log(response.config.data);
      
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  else {
    console.log(errors)
    reply.innerHTML = errors.join(', ')
  }
}

